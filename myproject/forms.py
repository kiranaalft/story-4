from django import forms
from .models import Schedule

class ScheduleForm(forms.ModelForm):
	class Meta:
		model = Schedule
		fields = [
			'name',
			'date',
			'place',
			'category',
			'notes'
		]
		
		labels = {
			'name':'Event Name:',
			'date':'Date:',
			'place':'Place:',
			'category':'Category:',
			'notes':'Notes:'
		}
		
		widgets = {
			'name':forms.TextInput(
				attrs = {
					'class':'form-control',
					'placeholder':'fill with your event name'
				}
			),
			'date':forms.DateTimeInput(
				attrs = {
					'class':'form-control',
					'placeholder':'yyyy-mm-dd'
				}
			),
			'place':forms.TextInput(
				attrs = {
					'class':'form-control',
					'placeholder':'where the event takes place'
				}
			),
			'category':forms.Select(
				attrs = {
					'class':'form-control'
				}
			),
			'notes':forms.Textarea(
				attrs = {
					'class':'form-control'
				}
			)
		}