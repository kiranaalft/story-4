from django.db import models

# Create your models here.

class Schedule(models.Model):
	name = models.CharField(max_length=100)
	date = models.DateTimeField(("Date & Time"), auto_now_add=False)
	place = models.CharField(max_length=100)
	LIST_CATEGORY = (
		('Assignment','Assignment'),
		('Class','Class'),
		('Committee Work','Committee Work'),
		('Daily Work','Daily Work'),
		('Organization','Organization'),
		('Quiz','Quiz')
	)
	category = models.CharField(
		max_length=100, choices = LIST_CATEGORY,
		default = 'Assignment'
	)
	notes = models.TextField(blank = True)
	
	def __str__(self):
		return "{}.{}".format(self.id, self.name)