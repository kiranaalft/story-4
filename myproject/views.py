from django.shortcuts import render, redirect
from .forms import ScheduleForm
from .models import Schedule 
# Create your views here.

def list(request):
	posts = Schedule.objects.all()
	
	context = {
		'page_title' : 'My Schedule',
		'posts':posts,
	}
	return render(request, 'list.html', context)
	
def delete(request, delete_id):
	Schedule.objects.filter(id=delete_id).delete()
	return redirect('myproject:schedule')
	
def create(request):
	sched_form = ScheduleForm(request.POST or None)
	if request.method == 'POST':
		if sched_form.is_valid():
			sched_form.save()
			return redirect('myproject:schedule')

	context = {
		'page_title' : 'Add Event',
		'sched_form':sched_form,
	}
	return render(request, 'create.html', context)
	
def home(request):
    return render(request, 'home.html')

def about(request):
    return render(request, 'about.html')

def profile(request):
    return render(request, 'profile.html')
	
def contacts(request):
    return render(request, 'contacts.html')
